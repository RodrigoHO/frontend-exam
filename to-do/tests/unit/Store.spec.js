import store from '../../src/store'

describe('Test save, delete and toggle functionalities', () => {
    let todo = null
    let todo2 = null
    beforeEach(() => {
        todo = {
          id: 1,
          order: 1,
          completed: true,
          description: 'Novo Todo'
        }
      })
    
    test('it should add a new todo', () => {
        store.dispatch('saveTodo', todo)
        store.commit('addTodo', todo)
        expect(store.state).toEqual({todos: [{id: 1, order: 1, completed: true, description: 'Novo Todo', quantity: 1}]})
    })

    test('it should change the todo Completed status', () => {
        let todoCompleted = todo.completed
        let toggleCompleted = !todoCompleted
        todo.completed = toggleCompleted;
        store.dispatch('updateTodoStatus', todo)
    })

    test('it should delete the todo ',() => {
        store.dispatch('deleteTodo', todo.id)
        expect(store.state.todos.length).toEqual(0)
    })


    
})
