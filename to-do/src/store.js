import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    todos: []
  },
  getters: {
    getPendingAmount: state =>
      state.todos.reduce(
        (acc, todo) => acc + todo.completed,
          0
      ),
    getTodos: state => state.todos,
  },
  mutations: {
    addTodo: (state, todo) => {
      state.todos.push({ ...todo, quantity: 1 });
    },
    
    removeTodo: (state, id) => {
      const idx = state.todos.findIndex(todo => todo.id === id);
      idx !== -1 && state.todos.splice(idx, 1);
    },

    toggleTodo: (state, id) => {
      let item = state.todos.find(todo => todo.id === id);
      item.completed = !item.completed;
    },
    setTodos (state, todos) {
      state.todos = todos
    }
  },
  actions: {
    loadTodos ({ commit }) {
      axios
        .get('http://localhost:3000/todos')
        .then(t => t.data)
        .then(todos => {
          commit('setTodos', todos)
      })
    },
    saveTodo ({ commit, state }, description) {
      if(description !== ''){
        const todo = {
          order: state.todos.length+1,
          completed: false,
          description: description,
        }
        axios
        .post('http://localhost:3000/todos', todo)
        .then(response => {
          commit('addTodo', response.data)
        }) 
      }  
    },
    deleteTodo({ commit, state }, id) {
      axios
        .delete(`http://localhost:3000/todos/${id}`)
        .then(commit('removeTodo', id))
    },
    updateTodoStatus({ commit, state}, todo){
      axios
        .patch(`http://localhost:3000/todos/${todo.id}`, {completed: !todo.completed})
        .then(commit('toggleTodo', todo.id))
    }
  },
  
});
